import logging
import pprint
import names
import random
import time
import uuid

from SOLIDserverRest import *
from SOLIDserverRest import adv as sdsadv

# ----------- parse args
try:
    import argparse
    parser = argparse.ArgumentParser(description='raspberry net probe system')

    parser.add_argument('--log', '-l', metavar='level', default='ERROR',
                        type=str, help='log level', nargs='?',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])

    parser.add_argument('--device', '-d', metavar='device',
                        type=str, help='name of the device',
                        nargs='?')

    parser.add_argument('--os', '-o', metavar='os',
                        type=str, help='os label',
                        nargs='?', default='')

    args = parser.parse_args()

except ImportError:
    logging.error('parse error - exit')
    exit()

_logFormat = '%(asctime)-15s [%(levelname)s] %(filename)s:%(lineno)d - %(message)s'
logLevel = logging.ERROR

if args.log == 'INFO':
    logLevel = logging.INFO
if args.log == 'DEBUG':
    logLevel = logging.DEBUG
if args.log == 'WARNING':
    logLevel = logging.WARNING
if args.log == 'ERROR':
    logLevel = logging.ERROR

logging.basicConfig(format=_logFormat, level=logLevel)

if args.device is None:
    logging.error("device needed")
    exit()

if args.os is None:
    logging.error("os needed")
    exit()

# configuration - to be adapted
SDS_HOST = "192.168.16.117"
SDS_LOGIN = "ipmadmin"
SDS_PWD = "admin"

logging.debug("create a connection to the SOLIDserver")
sds = sdsadv.SDS(ip_address=SDS_HOST,
                 user=SDS_LOGIN,
                 pwd=SDS_PWD)

try:
    sds.connect(method="basicauth")
except SDSError as e:
    logging.error(e)
    exit()

logging.debug(sds)

device_name = args.device
dev = sdsadv.Device(sds=sds,
                    name=device_name)
try:
    dev.refresh()
except SDSDeviceError as e:
    logging.error("device not found {}".format(device_name))
    exit()

classparams = {
    'os_version': args.os,
    'ansible_update': time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()),
}
dev.set_class_params(classparams)
dev.update()
