resource "docker_image" "centos8" {
  name = "ansible-target-centos8"
  keep_locally = true
}

resource "docker_image" "centos78" {
  name = "ansible-target-centos78"
  keep_locally = true
}

resource "docker_image" "centos77" {
  name = "ansible-target-centos77"
  keep_locally = true
}

resource "docker_image" "centos76" {
  name = "ansible-target-centos76"
  keep_locally = true
}

resource "docker_network" "ansible_test" {
  name = "ansible_test"
  check_duplicate = true
  driver = "bridge"
  ipam_config {
     subnet = "172.18.128.0/24"
  }
}

resource "docker_container" "ansible8" {
   count = var.servers80

   name  = "ansible-target-80${count.index}"
   image = docker_image.centos8.latest
   rm = false

   networks_advanced {
     name = docker_network.ansible_test.name
     ipv4_address = solidserver_ip_address.docker80[count.index].address
   }

   memory = 100
   memory_swap = 150

   provisioner "local-exec" {
    command = "sh add-ssh-key.sh ${solidserver_ip_address.docker80[count.index].address}"
  }
}

resource "docker_container" "ansible78" {
   count = var.servers78

   name  = "ansible-target-78${count.index}"
   image = docker_image.centos78.latest
   rm = false

   networks_advanced {
     name = docker_network.ansible_test.name
     ipv4_address = solidserver_ip_address.docker78[count.index].address
   }

   memory = 100
   memory_swap = 150

   provisioner "local-exec" {
    command = "sh add-ssh-key.sh ${solidserver_ip_address.docker78[count.index].address}"
  }
}

resource "docker_container" "ansible77" {
   count = var.servers77

   name  = "ansible-target-77${count.index}"
   image = docker_image.centos77.latest
   rm = false

   networks_advanced {
     name = docker_network.ansible_test.name
     ipv4_address = solidserver_ip_address.docker77[count.index].address
   }

   memory = 100
   memory_swap = 150

   provisioner "local-exec" {
    command = "sh add-ssh-key.sh ${solidserver_ip_address.docker77[count.index].address}"
  }
}

resource "docker_container" "ansible76" {
   count = var.servers76

   name  = "ansible-target-76${count.index}"
   image = docker_image.centos76.latest
   rm = false

   networks_advanced {
     name = docker_network.ansible_test.name
     ipv4_address = solidserver_ip_address.docker76[count.index].address
   }

   memory = 100
   memory_swap = 150

   provisioner "local-exec" {
    command = "sh add-ssh-key.sh ${solidserver_ip_address.docker76[count.index].address}"
  }
}

#output "net" {
#  value = docker_container.t01.network_data[0].ip_address
#}

