# EfficientIP SOLIDserver Ansible module
This Ansible module allows to easily interact with EfficientIP's SOLIDserver REST API leveraging the existing Python library [SOLIDserverRest](https://gitlab.com/efficientip/solidserverrest). It allows to manage supported resources through CRUD operations for efficient DDI orchestration.

<p align="center">
  <a align="center" href="https://www.efficientip.com/resources/video-what-is-ddi/">
    <img width="560" src="https://i.ytimg.com/vi/daQ0xEWNvYY/maxresdefault.jpg" title="What is DDI ?">
  </a>
</p>

This module is compatible with [SOLIDserverRest](https://gitlab.com/efficientip/solidserverrest) version 1.6.1 or greater.

# Install
Ansible automatically loads all executable files found in certain directories as modules, so you can create or add a local module in any of these locations:

* The following shared directory '/usr/share/ansible/plugins/modules/'
* Any directory added to the ANSIBLE_LIBRARY environment variable
* User's specific directory '~/.ansible/plugins/modules/'

In order to use the provided module, copy it in one the listed directory.

# Usage
This module can be used to improve your Ansible playbooks and scripts.

## Use case
### Deploy EC2 instance, then update SOLIDserver IPAM [EC2toSDS.yaml]
Variable declaration:
```
  vars:
    machine_ip_addr: '{{ myinstance["instances"][0]["public_ip_address"] }}'
```

EC2 instance deployment:
```
    - name: Create EC2 instance and gat IP
      ec2_instance_facts:
        ec2_region: instance_region
        aws_access_key: YOUR_AWS_ACCESS_KEY
        aws_secret_key: YOUR_AWS_SECRET
        instance_ids:
          - i-your_instance_id
      register: myinstance

    - debug: msg="IP of the instances = {{ myinstance["instances"][0]["public_ip_address"] }}" #echo of the sript
```

SOLIDserver IPAM update:
```
    - name: SOLIDserver ANSIBLE module => Register Instance's IP in IPAM
      sds_module:
        sds_host: '123.456.789.123' #IP of your SDS server
        sds_user: user #user used to connect at your SDS server
        sds_pass: userPass #password of the user used to connect at your SDS server
        sds_auth_method: native
        sds_service: ip_address_create #API service called
        sds_service_params:
          hostaddr: '{{ machine_ip_addr}}'
          site_name: Local
          name: VM_EC2

      register: sds_out

    - debug: var=sds_out.debug_out #echo return of API
```

# Methods that could be used
Methods are organized to match the ontology used in SOLIDServer, you will find:
* IPAM:
	* Sites - address spaces
	* Subnets (v4 and v6)
	* Pools (v4 and v6)
	* Addresses (v4 and v6)
	* Aliases (v4 and v6)
* DNS RR
* GSLB
* DHCP:
	* Scopes (v4 and v6)
	* Shared network
	* Ranges (v4 and v6)
	* Statics (v4 and v6)


More information about supported methods in the [specific document of the Python Lib](https://gitlab.com/efficientip/solidserverrest/blob/master/docs/METHODS.md)
