#!/usr/bin/python
# -*-coding:Utf-8 -*
##########################################################
"""
ansible raw module for EfficientIP SOLIDserver create/add/modify
API functions
"""

from __future__ import absolute_import, division, print_function
import json
from SOLIDserverRest import SOLIDserverRest, SERVICE_MAPPER
from ansible.module_utils.basic import AnsibleModule

ANSIBLE_METADATA = {'status': ['preview'],
                    'supported_by': 'EfficientIP community',
                    'version': '0.2'}

DOCUMENTATION = """
---
module: sds_ip_address_create.py
author: 
  - Alex Chauvin
short_description: create an IP address in SOLIDserver IPAM
requirements:
  - Python Lib for SOLIDserver: SOLIDserverRest v2.0 and upper
description:
  - create an IP address in SOLIDserver IPAM
extends_documentation_fragment: nothing
options:
  host:
    description:
      - SOLIDserver manager IP address or name
    type: str
    required: true
"""

__metaclass__ = type

# --------------------------------------------------------
def main():
    """ main function for ansible module """
    module_args = dict(
        sds_host=dict(required=True),
        sds_user=dict(required=True),
        sds_password=dict(required=True, no_log=True),
        site_name=dict(required=False, default='Local'),
        name=dict(required=False),
        ipv4=dict(required=True),
        mac_addr=dict(required=False),
        params=dict(required=False, type='dict')
    )

    result = dict(
        changed=False,
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # get variables from playbook
    sds_host = module.params['sds_host']
    sds_user = module.params['sds_user']
    sds_pass = module.params['sds_password']

    site_name = module.params['site_name']

    sds_service = 'ip_address_create'
    sds_parameters = module.params['params']

    ipv4 = module.params['ipv4']
    mac_addr = module.params['mac_addr']
    name = module.params['name']

    result['call_params'] = {
        'sds_host': sds_host,
        'service': sds_service,
        'site_name': site_name,
        'ipv4': ipv4,
    }

    if sds_parameters:
        result['call_params']['service_args'] = sds_parameters
    else:
        sds_parameters = {}

    if mac_addr:
        result['call_params']['mac_addr'] = mac_addr
        sds_parameters['mac_addr'] = mac_addr

    if name:
        result['call_params']['name'] = name
        sds_parameters['name'] = name

    sds_parameters['hostaddr'] = ipv4
    sds_parameters['site_name'] = site_name
        
    result['a'] = sds_parameters

    # CONNECTION
    con = SOLIDserverRest(sds_host)
    con.use_basicauth_sds(sds_user, sds_pass)
    con.set_ssl_verify(False)

    if module.check_mode:
        module.exit_json(**result)

    # QUERY
    sds_answer = con.query(sds_service, sds_parameters)

    try:
        rjson = sds_answer.json()
    except json.JSONDecodeError:   # pragma: no cover
        module.fail_json(msg='no json in return from SOLIDserver',
                         **result)

    for label in ['errno',
                  'errmsg',
                  'severity',
                  'category',
                  'ip_name',
                  'site_name'
                  'hostaddr',
                  'param_format',
                  'param_value',
                  'ret_oid'
                 ]:
        if label in rjson:
            result[label] = rjson[label]

    result['status_code'] = sds_answer.status_code

    if sds_answer.status_code == 200:
        result['changed'] = True

    if sds_answer.status_code == 201:
        result['changed'] = True
        result['created'] = True

    if sds_answer.status_code >= 300:
        module.fail_json(msg='error from SOLIDserver', **result)

    module.exit_json(**result)

main()
